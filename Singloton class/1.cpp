/*
 * 1.cpp
 *
 *  Created on: 14-Aug-2020
 *      Author: admin
 */



#include <iostream>
using namespace std;
class Singleton
{
    private:

        static Singleton* instance;


        Singleton();

    public:

        static Singleton* getInstance();
};


Singleton* Singleton::instance = 0;

Singleton* Singleton::getInstance()
{
    if (instance == 0)
    {
        instance = new Singleton();
    }

    return instance;
}

Singleton::Singleton()
{}

int main()
{

    Singleton* s = Singleton::getInstance();
    Singleton* r = Singleton::getInstance();


    cout << s << std::endl;
    cout << r << std::endl;
}

