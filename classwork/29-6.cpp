/*
 * 29-6.cpp
 *
 *  Created on: 30-Jul-2020
 *      Author: admin
 */
#include<iostream>
using namespace std;

#define SIZE	3
class Array
{
private:
	int arr[ SIZE ];
public:
	Array( void )
	{
		for( int index = 0; index < SIZE; ++ index )
			this->arr[ index ] = 0;
	}

	}
	int getElement( int index )
	{
		return this->arr[ index ];
	}
	void setElement( int index, int element )
	{
		this->arr[ index ] = element;
	};

};
void accept_record( Array *ptr )
{
	int element;
	for( int index = 0; index < SIZE; ++ index )
	{
		cout<<"Enter element	:	";
		cin>>element;
		ptr->setElement( index, element );
	}
}
void print_record( Array *ptr )
{
	for( int index = 0; index < SIZE; ++ index )
		cout<<ptr->getElement(index);
}
int main( void )
{
	Array a1;

	accept_record( &a1 );

	print_record( &a1 );

	return 0;
}
