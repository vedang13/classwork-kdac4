///*
// * 3.cpp
// *
// *  Created on: 19-Aug-2020
// *      Author: admin
// */
//
//
//
//
//#include<iostream>
//using namespace std;
//class A
//{
//private:
//	int num1;
//public:
//	A( void )
//	{
//		//cout<<"A( void )"<<endl;
//		this->num1 = 10;
//	}
//	A( int num1 )
//	{
//		//cout<<"A( int num1 )"<<endl;
//		this->num1 = num1;
//	}
//	void printRecord( void )
//	{
//		cout<<"Num1	:	"<<this->num1<<endl;
//	}
//	~A( void )
//	{
//		//cout<<"~A( void )"<<endl;
//	}
//};
//class B : public A
//{
//private:
//	int num2;
//public:
//	B( void )
//	{
//		//cout<<"B( void )"<<endl;
//		this->num2 = 20;
//	}
//	B( int num2 )
//	{
//		//cout<<"B( int num2 )"<<endl;
//		this->num2 = num2;
//	}
//	B( int num1, int num2 ) : A( num1 )
//	{
//		//cout<<"B( int num1, int num2 )"<<endl;
//		this->num2 = num2;
//	}
//	void printRecord( void )
//	{
//		A::printRecord();
//		cout<<"Num2	:	"<<this->num2<<endl;
//	}
//	~B( void )
//	{
//		//cout<<"~B( void )"<<endl;
//	}
//};
//class C : public A
//{
//private:
//	int num3;
//public:
//	C( void )
//	{
//		//cout<<"C( void )"<<endl;
//		this->num3 = 30;
//	}
//	C( int num3 )
//	{
//		//cout<<"C( int num3 )"<<endl;
//		this->num3 = num3;
//	}
//	C( int num1, int num3 ) : A( num1 )
//	{
//		//cout<<"C( int num1, int num3 )"<<endl;
//		this->num3 = num3;
//	}
//	void printRecord( void )
//	{
//		A::printRecord();
//		cout<<"Num3	:	"<<this->num3<<endl;
//	}
//	~C( void )
//	{
//		//cout<<"~C( void )"<<endl;
//	}
//};
//int main( void )
//{
//	C obj(500);
//	obj.printRecord();
//	return 0;
//}
//
//
