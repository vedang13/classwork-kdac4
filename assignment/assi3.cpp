/*
 * assi3.cpp
 *
 *  Created on: 28-Jul-2020
 *      Author: admin
 */


#include <iostream>
#include <cstring>

using namespace std;


class Account{
	char name[50];
	int accountNum;
	char type[50];
	float balance;
	static int setAccountNum;
public:



	void acceptRecord()
	{
		cout<<"Enter name:		"<<endl;
		cin>>this->name;
		cout<<"Enter type	:		"<<endl;
		cin>>this->type;
		cout<<"Enter Balance:		"<<endl;
		cin>>this->balance;
		this->accountNum = ++this->setAccountNum;
		cout<<"Your account have been succesfully created with an account num "<<this->accountNum<<endl;

	}
	void printRecord()
	{
		cout<<"Name:	"<<this->name<<endl;
		cout<<"Account num:	"<<this->accountNum<<endl;
		cout<<"Type:	"<<this->type<<endl;
		cout<<"Balance:	"<<this->balance<<endl;
	}
	friend class Bank;
};
 int Account::setAccountNum = 100;

 class Bank{
	static int index;
	static int size;
	Account arr[5];
public:

	void createAccount()
	{
		this->arr[++this->index].acceptRecord();
	}
	void withdraw(int accountNum,float amt)
	{
		for(int i = 0;i<this->size;i++)
		{
			if(accountNum == this->arr[i].accountNum)
			{
				this->arr[i].balance -= amt;
				cout<<"Withdraw successfull"<<endl;
				return ;
			}
		}
		cout<<"Account number no valid please enter valid account num"<<endl;
	}
	void deposit(int accountNum,float amt)
	{
		for(int i = 0;i<this->size;i++)
		{
			if(accountNum == this->arr[i].accountNum)
			{
				this->arr[i].balance += amt;
				cout<<"Amount have been deposited into your account "<<endl;
				return ;
			}
		}
		cout<<"Account number no valid please enter valid account num"<<endl;
	}
	void display(int accountNum)
	{
		for(int i = 0;i<this->size;i++)
		{
			if(accountNum == this->arr[i].accountNum)
			{
				this->arr[i].printRecord();

				return ;
			}
		}
		cout<<"Account number no valid please enter valid account num"<<endl;
	}

};
 int Bank::size = 5;
int Bank::index = -1;

int menuList()
{
	int choice;
	cout<<"0.Exit"<<endl;
	cout<<"1.Create account"<<endl;
	cout<<"2.Deposit Money"<<endl;
	cout<<"3.Withdraw Money"<<endl;
	cout<<"4.Print account details"<<endl;
	cout<<"Enter your choice:	"<<endl;
	cin>>choice;
	return choice;
}

void acceptAccountNum(int &n)
{
	cout<<"Enter number:	"<<endl;
	cin>>n;
}
void acceptAmt(float &n)
{
	cout<<"Enter amount:	"<<endl;
	cin>>n;
}
int main()
{
int choice;
Bank b;
while((choice = menuList())!=0)
	{
		switch(choice)
		{
		case 1:
			{
				b.createAccount();
				break;
			}
		case 2:
			{
				int n;
				float amt;
				acceptAccountNum(n);
				acceptAmt(amt);
				b.deposit(n,amt);
				break;
			}
		case 3:
		{
			int n;
			float amt;
			acceptAccountNum(n);
			acceptAmt(amt);
			b.withdraw(n,amt);
			break;
		}
		case 4:
			int n;
			acceptAccountNum(n);
			b.display(n);
			break;

		}
	}

	return 0;
}
