///*
// * 4.cpp
// *
// *  Created on: 06-Aug-2020
// *      Author: admin
// */
//
//
//
//#include<iostream>
//#include<string>
//using namespace std;
//class IllegalArgumentException
//{
//private:
//	string message;
//public:
//	IllegalArgumentException( string message = "Illegal Argument Exception" ) : message ( message )
//	{	}
//	string getMessage( void )const throw( )
//	{
//		return this->message;
//	}
//};
//class ArrayIndexOutOfBoundsException
//{
//private:
//	string message;
//public:
//	ArrayIndexOutOfBoundsException( string message = "Array Index Out Of Bounds Exception") : message ( message )
//	{	}
//	string getMessage( void )const throw( )
//	{
//		return this->message;
//	}
//};
//class Array
//{
//private:
//	int size;
//	int *arr;
//public:
//	Array( void )throw( )
//	{
//		this->size = 0;
//		this->arr = NULL;
//	}
//	Array( int size )throw( bad_alloc )
//	{
//		this->size = size;
//		this->arr = new int[ this->size ];
//	}
//	int getElement( int index )const throw( ArrayIndexOutOfBoundsException )
//	{
//		if( index < 0 || index > this->size )
//			throw ArrayIndexOutOfBoundsException("Invalid index ");
//		return this->arr[ index ];
//	}
//	void setElement( const int index, const int element )throw( ArrayIndexOutOfBoundsException)
//	{
//		if( index < 0 || index > this->size )
//			throw ArrayIndexOutOfBoundsException("Invalid index ");
//		this->arr[ index ] = element;
//	}
//	int getSize( void )const throw( )
//	{
//		return this->size;
//	}
//	void setSize( int size )throw( IllegalArgumentException, bad_alloc )
//	{
//		if( size < 0 )
//			throw IllegalArgumentException("Invalid size");
//		this->~Array();
//		if( size == 0 )
//			this->size = 0;
//		else
//		{
//			this->size = size;
//			this->arr = new int[ this->size ];
//		}
//	}
//	~Array( void )throw( )
//	{
//		if( this->arr != NULL )
//		{
//			delete[] this->arr;
//			this->arr = NULL;
//		}
//	}
//};
//void accept_record( Array &a1 )
//{
//	int element ;
//	for( int index = 0; index < a1.getSize(); ++ index )
//	{
//		cout<<"Enter element	:	";
//		cin>>element;
//		a1.setElement(index, element );
//	}
//}
//void print_record( Array &a1 )
//{
//	int element ;
//	for( int index = 0; index < a1.getSize(); ++ index )
//	{
//		element = a1.getElement( index );
//		cout<<"Element	:	"<<element<<endl;
//	}
//}
//int main( void )
//{
//	Array a1( 3 );
//	accept_record( a1 );
//	print_record( a1 );
//
//	a1.setSize(5);
//
//		accept_record( a1 );
//		print_record( a1 );
//	return 0;
//}
//
//
//
