/*
 * 8.cpp
 *
 *  Created on: 06-Aug-2020
 *      Author: admin
 */

#include<cstring>
#include<iostream>
using namespace std;
class Complex
{
private:
	int real;
	int imag;
public:
	Complex( int real = 0, int imag = 0 )
	{
		this->real = real;
		this->imag = imag;
	}
	void printRecord( void )
	{
		cout<<"Real Number	:	"<<this->real<<endl;
		cout<<"Imag Number	:	"<<this->imag<<endl;
	}
};


int main( void )
{
	int num1 = 10;
	int num2 = num1;
}
