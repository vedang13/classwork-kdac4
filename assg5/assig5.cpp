/*
 * assig5.cpp
 *
 *  Created on: 04-Aug-2020
 *      Author: admin
 */






#include <iostream>
#include <vector>
using namespace std;

class Account{
	static int count;
	int account_num;
	string name;
	string type;
	float balance;
	public:
	Account()
	{
		this->name =  " ";
		this->type = " ";
		this->balance = 0.0;
		this->account_num = 0;
	}

	int getAccountNum()
	{
		return this->account_num;
	}
	string getName()
	{
		return this->name;
	}
	string getType()
	{
		return this->type;
	}
	float getBalance()
	{
		return this->balance;
	}

	void setName(const string &name)
	{
		this->name = name;
	}
	void setType(const string &type)
	{
		this->type = type;
	}
	void setBalance(const float &b)
	{
		this->balance = b;
	}
	void setAccountNUm()
	{
		this->account_num = ++this->count;
	}
};
int Account::count = 1000;

class Bank{
	vector<Account> acc;
public:

	void setObj(Account &a)
	{
		this->acc.push_back(a);
	}

	vector<Account>& getVector()
	{
		return this->acc;
	}
};

void acceptRecord(Bank &b)
{
	Account a;
	cout<<"Enter Name:	"<<endl;
	string name;
	cin>>name;
	a.setName(name);
	cout<<"Enter Type:	"<<endl;
	string type;
	cin>>type;
	a.setType(type);
	cout<<"Enter Balance:	"<<endl;
	float balance;
	cin>>balance;
	a.setBalance(balance);
	a.setAccountNUm();
	int num = a.getAccountNum();
	cout<<"Congratulations your account have been created with account num :	"<<num<<endl;
	b.setObj(a);
}

void printRecord(Bank &b,int num)
{
	float balance;
	string name,type;
	vector<Account> &brr = b.getVector();
	vector<Account> ::iterator itrstart = brr.begin();
	vector<Account> ::iterator itrend = brr.end();
	while(itrstart != itrend)
	{
		if(itrstart->getAccountNum()==num)
		{
		name = itrstart->getName();
		type = itrstart->getType();
		balance = itrstart->getBalance();
		cout<<"Name :	"<<name<<endl;
		cout<<"Account Number :	"<<num<<endl;
		cout<<"Balance :	"<<balance<<endl;
		cout<<"Type :	"<<type<<endl;
		return;
		}
		itrstart++;
	}
	cout<<"Account number is not valid"<<endl;
}
void deposit(float amount,int num,Bank &b)
{
	float balance;
	vector<Account> &brr = b.getVector();
	vector<Account> ::iterator itrstart = brr.begin();
	vector<Account> ::iterator itrend = brr.end();
	while(itrstart != itrend)
	{
		if(itrstart->getAccountNum()==num)
		{
			balance = itrstart->getBalance();
			balance  += amount;
			itrstart->setBalance(balance);
			return;
		}
		itrstart++;
	}
	cout<<"Account number is not valid"<<endl;
}
void withdraw(float amount,int num,Bank &b)
{
	float balance;
	vector<Account> &brr = b.getVector();
	vector<Account> ::iterator itrstart = brr.begin();
	vector<Account> ::iterator itrend = brr.end();
	while(itrstart != itrend)
	{
		if(itrstart->getAccountNum()==num)
		{
			balance = itrstart->getBalance();
			balance  -= amount;
			if(itrstart->getBalance() > 0)
			{
				itrstart->setBalance(balance);
			}
			else
				cout<<"Cant Process because you have limited balance:	"<<itrstart->getBalance()<<endl;
			return;
		}
		itrstart++;
	}
	cout<<"Account number is not valid"<<endl;
}

int menuList()
{
	int choice;
	cout<<"0.Exit"<<endl;
	cout<<"1.Create account"<<endl;
	cout<<"2.Withdraw money"<<endl;
	cout<<"3.Deposit money"<<endl;
	cout<<"4.Print account"<<endl;
	cout<<"Enter choice"<<endl;
	cin>>choice;
	return choice;
}
void acceptAmount(float &a)
{
	cout<<"Enter amount:	"<<endl;
	cin>>a;
}
void acceptNum(int &n)
{
	cout<<"Enter account num:	"<<endl;
	cin>>n;
}
int main()
{
	Bank b;
	int choice,account_num;
	float amt;
	while((choice=menuList())!=0)
	{
		switch(choice)
		{
		case 1:
			::acceptRecord(b);
			break;
		case 2:
			::acceptAmount(amt);
			::acceptNum(account_num);
			::withdraw(amt,account_num,b);
			break;
		case 3:
			::acceptAmount(amt);
			::acceptNum(account_num);
			::deposit(amt,account_num,b);
			break;
		case 4:
			::acceptNum(account_num);
			::printRecord(b,account_num);
			break;
		}
	}

	return 0;
}

