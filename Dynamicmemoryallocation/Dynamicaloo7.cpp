/*
 * Dynamicaloo7.cpp
 *
 *  Created on: 05-Aug-2020
 *      Author: admin
 */


#include<iostream>

using namespace std;

class Complex
{
private :
	int real;
	int imag;
public:
	Complex():real(0),imag(0)
{
		cout<<"Complex (void)"<<endl;
}

	Complex(int real,int imag)
	{
		this->real=real;
		this->imag=imag;
	}

	void printRecord(void)
	{
		cout<<"Real no is :"<<this->real<<endl;
		cout<<"Imag no is :"<<this->imag<<endl;
	}

	~Complex(void)
	{
		cout<<"~Complex( void )"<<endl;
	}
};

int main()
{
	Complex *ptr = new Complex[ 3 ];
		for( int index = 0; index < 3; ++ index )

			ptr[ index ].printRecord();
		delete[] ptr;
		ptr = NULL;
		return 0;
}
