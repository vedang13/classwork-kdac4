///*
// * 6.cpp
// *
// *  Created on: 04-Aug-2020
// *      Author: admin
// */
//#include<iostream>
//using namespace std;
//int main( void )
//{
//	//Memory allocation and deallocation for single variable
//
//	int *ptr = new int;	//Memory allocation
//	//int *ptr = (int*)operator new(sizeof(int));
//
//	*ptr = 125;	//dereferencing
//	cout<<"Value	:	"<<*ptr<<endl;//dereferencing
//
//	delete ptr;	//Memory deallocation
//	//operator delete(ptr);
//
//	ptr = NULL;
//	return 0;
//}
