/*
 * Vehicle.h
 *
 *  Created on: 22-Aug-2020
 *      Author: admin
 */

#ifndef VEHICLE_H_
#define VEHICLE_H_


#ifndef VEHICLE_H_
#define VEHICLE_H_
#include<string>
#include<vector>
#include<sstream>
#include"../Include/InvalidVehicle.h"
#include<iostream>
#include<fstream>
#include<map>
#include"../Include/FileError.h"
#include<algorithm>

using namespace std;
namespace kd4
{
class Vehicle
{
	string company;
	string model;
	string number;
	string mobileNum;
public:
	Vehicle(string company=" ",string model = " ",string number= " ",string mobileNum = " ");
	Vehicle(const Vehicle &other);
	Vehicle& operator=(const Vehicle &other);
	string getCompany() const ;
	void setCompany(const string &company) ;
	string getModel() const ;
	void setModel(const string &model);
	string getNumber() const ;
	void setNumber(const string &number) ;
	void acceptRecord(string mobileNum = " ");
	void printRecord();
	void setMobileNum(string &mobileNum);
	string getMobileNum() ;
	void storeRecord();
	static vector<Vehicle>&  readRecord();
	friend ostream& operator<<(ostream &cout,Vehicle &other);
	friend istream& operator>>(istream &cin,Vehicle &other);
};
}





#endif /* VEHICLE_H_ */
