/*
 * Part.h
 *
 *  Created on: 22-Aug-2020
 *      Author: admin
 */

#ifndef PART_H_
#define PART_H_

#include<string>
#include<iostream>
#include<list>
using namespace std;
namespace kd4{
class Part{
	string desc;
	double rate;
public:
	Part(string desc = " ",double rate = 0.0);
	void acceptRecord();
	friend iostream& operator>>(iostream& cin,Part &other);
	void print();
	friend ostream& operator<<(ostream& cout,Part &o);
	string getDesc() const;
	void setDesc(const string &desc);
	double getRate() const;
	void setRate(double rate);
	void storePartRecord();
	static void fetchRecord(list<Part> &partList);
};
}






#endif /* PART_H_ */
