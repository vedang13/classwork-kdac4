/*
 * FileError.h
 *
 *  Created on: 22-Aug-2020
 *      Author: admin
 */

#ifndef FILEERROR_H_
#define FILEERROR_H_

#include<string>
using namespace std;
namespace kd4{
class FileHandlingError{
	string message;
public:
	FileHandlingError(string mes);
	string getMessage();
};
}



#endif /* FILEERROR_H_ */
