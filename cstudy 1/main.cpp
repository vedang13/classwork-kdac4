/*
 * main.cpp
 *
 *  Created on: 22-Aug-2020
 *      Author: admin
 */





#include"../Include/Customer.h"
#include<list>
#include"../Include/Part.h"
using namespace kd4;
using namespace std;
class Service{
	string desc;
public:
	Service(string desc = " "):desc(desc)
{}
	Service(const Service &o)
	{
		this->desc = o.desc;
	}
	virtual int acceptRecord()
	{
		cout<<"Enter Description:		";
		cin>>desc;
		return 1;
	}
	virtual void print()
	{
		cout<<"Description:		"<<this->desc<<endl;
	}
	virtual double price()=0;
	string getDesc() const {
		return desc;
	}

	void setDesc(const string &desc) {
		this->desc = desc;
	}
	virtual ~Service()
	{

	}
};
class Oil :public Service
{
	double cost;
public:
	Oil(string desc = " ",double cost = 0.0):Service(desc),cost(cost)
{}
	Oil(const Oil &other)
	{
		this->cost = other.cost;
	}
	virtual  int  acceptRecord()
	{
		 double rate;
		Service::acceptRecord();
		cout<<"Enter Cost:	";
		cin>>this->cost;
		return 1;
	}
	virtual void print()
	{
		Service::print();
		cout<<"Cost:			"<<this->cost<<endl;
	}
	virtual double price()
	{
		return this->cost;
	}
	double getCost() const {
		return cost;
	}

	void setCost(double cost) {
		this->cost = cost;
	}
	virtual ~Oil()
		{

		}
};
class Maintainace : public Service
{
	double laberCharges;
	list<Part> partList;
public:
	Maintainace(double p = 0.0):laberCharges(p)
{
		//Part::fetchRecord(partList);
}
	void addPart()
	{
		Part p;
		p.acceptRecord();
		this->partList.push_back(p);
		cout<<"Part added to list"<<endl;
	}
	int menuList()
	{
		int ch;
		cout<<"0.Previous Menu"<<endl;
		cout<<"1.Select Part"<<endl;
		cout<<"2.Labour charges"<<endl;
		cout<<"Enter choice:	";
		cin>>ch;
		return ch;

	}
	virtual int acceptRecord()
	{
		Part p;
		int ch;
		while((ch=this->menuList())!=0)
		{
			Service::acceptRecord();
			switch(ch)
			{
			case 1:
				this->addPart();
				break;
			case 2:
				cout<<"Enter Laber charges:		";
				cin>>this->laberCharges;
				break;
			}
		}
		return 1;
	}
	virtual void print()
	{
		cout<<"Laber charges:	"<<this->laberCharges<<endl;
		Service::print();
		cout<<"Parts :	"<<endl;
		for(Part p:this->partList)
			p.print();
	}

	virtual double price()
	{
		double price;
		list<Part>::iterator itrstart = this->partList.begin();
		list<Part>::iterator itrend = this->partList.end();
		{
			price += itrstart->getRate();
		}
		price += this->laberCharges;
		return price;
	}
	double getLaberCharges() const {
		return laberCharges;
	}

	void setLaberCharges(double laberCharges) {
		this->laberCharges = laberCharges;
	}

	const list<Part>& getPartList() const {
		return partList;
	}

	void setPartList(const list<Part> &partList) {
		this->partList = partList;
	}
	virtual ~Maintainace()
		{

		}
};

class ServiceRequest{
	string name;
	list<Service*> servList;
	string vehNumber;
public:

	ServiceRequest(string name = " ",string number = " "):name(name),vehNumber(number)
{}

	void processRequest()
	{
		Service *req = NULL;
		this->addItem(req);
	}
	int menuForItem()
	{
		int ch;
		cout<<"0.Prevoius Menu"<<endl;
		cout<<"1.Oil"<<endl;
		cout<<"2.Maintance"<<endl;
		cin>>ch;
		return ch;
	}
	void addItem(Service *req)
	{
		int ch;
		while((ch=menuForItem())!=0)
		{
			switch(ch)
			{
			case 1:
				req = new Oil();
				break;
			case 2:
				req=new Maintainace();
				break;
			}
			if(req!=NULL)
			{
				req->acceptRecord();
				this->servList.push_back(req);
			}
		}
	}
	void printService()
	{
		for(Service* s : this->servList)
			s->print();
	}
	string getName() const {
		return name;
	}

	void setName(const string &name) {
		this->name = name;
	}

	const list<Service*>& getServList() const {
		return servList;
	}

	void setServList(const list<Service*> &servList) {
		this->servList = servList;
	}

	string getVehNumber() const {
		return vehNumber;
	}

	void setVehNumber(const string &vehNumber) {
		this->vehNumber = vehNumber;
	}
};
class IncorrectAmount{
	string m;
	public:
	IncorrectAmount(string m = " "):m(m)
	{}
		string getMessage()
		{
			return this->m;
		}
};
class Bill
{
	static int billNo;
	double amount;
	double paidAmount;
	ServiceRequest *request;
public:
	Bill(ServiceRequest *request)
{
		this->amount = 0.0;
		this->paidAmount =  0.0;
		this->request = request;
}
	double computeAmount()
	{
		list<Service*> l = this->request->getServList();
		for(Service* ser:l)
		{
			this->amount += ser->price();
		}
		this->billNo++;
		return this->amount;
	}
	double computeTax()
	{
		double tax = this->amount * 0.12;
		return tax;
	}
	double computeFinalBill()
	{
		double tax = this->computeTax();
		this->amount += tax;
		return this->amount;
	}
	int getBillNumber()
	{
		return this->billNo;
	}
	double getAmount()
	{
		return this->amount;
	}
	void payBill(double amt)
	{
		double balanceAmount = this->amount - amt;
		if(balanceAmount > 0)
			throw IncorrectAmount("Insufficient amount ");
		this->paidAmount = amt;
	}
	void getBalanceAmount()
	{
		cout<<"Balance Amount is:	"<<this->amount-this->paidAmount<<endl;
	}
	void print()
	{
		cout<<"Bill Number:		"<<this->billNo<<endl;
		cout<<"Total Bill amount:	"<<this->amount<<endl;
		cout<<"Paid amount:		"<<this->paidAmount<<endl;
	}
};
int Bill::billNo = 0;
class DuplicateCustomer{
	string m;
public:
	DuplicateCustomer(string m = " "):m(m)
{}
	string getMessage()
	{
		return this->m;
	}
};
class ServiceStation
{
	vector<Customer> custList;
	vector<Bill> billList;
	vector<ServiceRequest> serv;
	static int serviceCount;
	string name;
	static ServiceStation *instance;
	ServiceStation(const string &name)
	{
		this->name = name;
	}
public:
	static ServiceStation* getinstace(const string &name)
	{
		if(!instance)
		{
			instance = new ServiceStation(name);
		}
		return instance;
	}

	void loadCustomerDetails()
	{
		Customer::readFromFile(this->custList);
	}
	void newCustomer()
	{
		Customer c;
		c.acceptRecord();
		Customer *cus = NULL;
		cus = this->findCustomer(c.getName(),c.getMobile());
		if(cus!=NULL)
		{
			throw DuplicateCustomer("Cusomer already exist");
			//Vehicle v;
			//vector<Vehicle> veh = c.getVehList();
			//this->custList[i].vehList.push_back(veh[0]);
		}
		else
		{
			this->custList.push_back(c);
			cout<<"New Customer details added."<<endl;
		}
	}
	void displayCustomerList()
	{
		for(Customer c:this->custList)
			cout<<c<<endl;
	}
	Customer* findCustomer(const string &name,string num)
	{
		static Customer c;
		for(size_t i = 0;i<this->custList.size();i++)
		{
			if(custList[i].getName()==name)
			{
				if(custList[i].getMobile()==num)
				{
					c = custList[i];
					return &c;
				}
			}
		}
		//throw InvalidVehicel("Customer not found");
		return NULL;
	}

	void handleServiceRequest()
	{
		ServiceRequest s;
		s.processRequest();
		this->serv.push_back(s);
	}
	void computeCash(Customer *c,Vehicle &v)
	{
		size_t sz = this->serv.size();
		ServiceRequest s = this->serv[--sz];
		Bill b(&s);
		cout<<"$"<<this->name<<"$"<<endl;
		double amount = b.computeAmount();
		cout<<"Bill Number:		"<<b.getBillNumber()<<endl;
		cout<<"Name:		"<<c->getName()<<endl;
		cout<<"Address:		"<<c->getAddress()<<endl;
		cout<<"Mobile:		"<<c->getMobile()<<endl;
		cout<<v<<endl;
		s.printService();
		cout<<"Amount without tax:	"<<amount<<endl;
		double taxAmount = b.computeTax();
		cout<<"12% tax:		"<<taxAmount<<endl;
		double finalAmount = b.computeFinalBill();
		cout<<"Final total is:		"<<finalAmount<<endl;
		this->billList.push_back(b);
	}
	const vector<Bill>& getBillList() const {
		return billList;
	}
	void searchBill(int num)
	{
		//static Bill *bil;
		double amount;
		for(Bill b:this->billList)
		{
			if(b.getBillNumber()==num)
			{
				//cout<<1<<endl;
				cout<<"Enter amount:	";
				cin>>amount;
				b.payBill(amount);
				return;
			}
		}
		cout<<"Bill not found"<<endl;
	}
	void searchBillForBalnce(int num)
	{
		//static Bill *bil;
		double amount;
		for(Bill b:this->billList)
		{
			if(b.getBillNumber()==num)
			{
				b.print();
				return;
			}
		}
		cout<<"Bill not found"<<endl;
	}

	void setBillList(const vector<Bill> &billList) {
		this->billList = billList;
	}
};
ServiceStation* ServiceStation::instance = 0;
int ServiceStation::serviceCount = -1;
int menuListForCustomer()
{
	int choice;
	cout<<"0.Exit"<<endl;
	cout<<"1.New customer register"<<endl;
	cout<<"2.Prevoiusly register "<<endl;
	cout<<"Enter Choice:	"<<endl;
	cin>>choice;
	return choice;
}
int menuListForService()
{
	int ch;
	cout<<"0.Previous Menu"<<endl;
	cout<<"1.Display Vehicle details"<<endl;
	cout<<"2.Choose Vehicle from list"<<endl;
	cout<<"3.Process New Service request"<<endl;
	cout<<"4.Prepare bill"<<endl;
	cout<<"5.Pay Bill"<<endl;
	cout<<"6.Show Balance Amount"<<endl;
	cout<<"Enter Choice:	";
	cin>>ch;
	return ch;
}
int menuListForVehicleChoice()
{
	int ch;
	cout<<"0.Previous Menu"<<endl;
	cout<<"1.Register New Vehicle"<<endl;
	cout<<"2.Select From Old List"<<endl;
	cout<<"Enter your Choice:	";
	cin>>ch;
	return ch;
}
int main()
{
	ServiceStation *s = ServiceStation::getinstace("Ajinkya Service Station");
	s->loadCustomerDetails();
	int choice,num,billNum;
	double amount;
	string name,mobNum;
	Customer *cus = NULL;
	Vehicle v;
	while((choice=menuListForCustomer())!=0)
	{
		try
		{
			switch(choice)
			{
			case 1:
				s->newCustomer();
				break;
			case 2:
				cout<<"Enter Registered name :		";
				getline(cin,name);
				while(name.length()==0)
					getline(cin,name);
				cout<<"Enter Registered Mobile Num :		";
				cin>>mobNum;
				cus = s->findCustomer(name,mobNum);
				break;
			}
		}
		catch(DuplicateCustomer &ex)
		{
			cout<<ex.getMessage()<<endl;
		}
		if(cus!=NULL)
		{
			while((choice=menuListForService())!=0)
			{
				try{

					switch(choice)
					{
					case 1:
						cus->printVehicle();
						break;
					case 2:
						while((choice=menuListForVehicleChoice())!=0)
						{
							switch(choice)
							{
							case 1:
								v= cus->newVehicle();
								break;
							case 2:
								cus->printVehicle();
								cout<<"Enter sr. no of vehicle which you want to choose:	";
								cin>>num;
								v = cus->getSingleVehicle(num-1);
								break;
							}
						}
						break;
					case 3:
						s->handleServiceRequest();
						break;
					case 4:
						s->computeCash(cus,v);
						break;
					case 5:
						cout<<"Enter Bill Number:	"<<endl;
						cin>>billNum;
						s->searchBill(billNum);
						break;
					case 6:
						cout<<"Enter Bill Number:	"<<endl;
						cin>>billNum;
						s->searchBillForBalnce(billNum);
						break;
					}
				}
				catch(IncorrectAmount &ex)
				{
					cout<<ex.getMessage()<<endl;
				}
			}
		}
	}


	/*
	s->newCustomer();
	if(cus)
	{
		cus->printVehicle();
	}
	cout<<"Choose Option:	";
	cout<<"1.Choose from above option"<<endl;
	cout<<"2.Enter New Vehicle"<<endl;
	int ch;
	Vehicle v;
	cin>>ch;
	if(ch==1)
	{
		cout<<"Enter Option of vehicle which you want:	";
		cin>>ch;

	}
	else
	{
		v = cus->newVehicle();
	}*/
	//s->computeCash(cus,v);

	//cout<<c<<endl;
	//c.acceptRecord();
	//cus.push_back(c);
	//cout<<c;

	//cout<<c.size()<<endl;
	//c[0].printRecord();
	//for(Customer cus:c)
		//cus.printRecord();
	return 0;
}
