/*
 * Customer.cpp
 *
 *  Created on: 17-Aug-2020
 *      Author: sunbeam
 */

#include"../Include/Customer.h"
#include"../Include/FileError.h"
using namespace kd4;
namespace kd4{

Customer::Customer(string name ,string address ,string mobile ):name(name),address(address),mobile(mobile)
{}

	string Customer::getAddress() const {
		return address;
	}

	void Customer::setAddress(const string &address) {
		this->address = address;
	}

	string Customer::getMobile() const {
		return mobile;
	}

	void Customer::setMobile(const string &mobile)
	{
		if(mobile.size()<10)
			throw ;
		this->mobile = mobile;
	}

	string Customer::getName() const {
		return name;
	}

	void Customer::setName(const string &name) {
		this->name = name;
	}

	const vector<Vehicle>& Customer::getVehList() const {
		return vehList;
	}

	void  Customer::setVehList(const vector<Vehicle> &vehList) {
		this->vehList = vehList;
	}


	Vehicle& Customer::newVehicle()
	{
		Vehicle v;
		cout<<"Enter Vehicle details:	"<<endl;
		v.acceptRecord(this->mobile);
		this->vehList.push_back(v);
		cout<<"Vehicle details Loaded"<<endl;
		size_t sz = this->vehList.size();
		return this->vehList[--sz];
	}

	void Customer::printVehicle()
	{
		for(Vehicle v:this->vehList)
			cout<<v<<endl;
	}
	void Customer::printRecord()
	{
		cout<<"Name :			"<<this->name<<endl;
		cout<<"Address :		"<<this->address<<endl;
		cout<<"Mobile :		"<<this->mobile<<endl;
		printVehicle();
	}

	void Customer::writeInFile( Customer &c)
	{
		ofstream file;
		file.open("customer.csv",ios::out | ios::app);

		if(!file)
		{
			throw FileHandlingError("Error in file opening") ;
		}
		else
		{

			size_t i = c.getVehList().size();
			cout<<i<<endl;
			file<<c.getName()<<","<<c.getAddress()<<","<<c.getMobile()<<"\n";
			c.vehList[--i].storeRecord();
			cout<<"Record saved successfully.."<<endl;
		}
	}
	Vehicle& Customer::getSingleVehicle(int i)
		{
			return this->vehList[i];
		}
	void Customer::readFromFile(vector<Customer> &c)
	{
		ifstream file;
		file.open("customer.csv");
		if(!file)
		{
			throw FileHandlingError("Error in file opening") ;
		}
		else
		{
			string line;
			while(getline(file,line))
			{
				string token[3];
				stringstream str(line);
				//cout<<line<<endl;
				for(int i =0;i<3;i++)
				{
					getline(str,token[i],',');
				}
				Customer cus(token[0],token[1],token[2]);
				c.push_back(cus);
			}
		}
		vector<Vehicle> v = Vehicle::readRecord();
		for(size_t j = 0;j<c.size();j++)
		{
			for(size_t i = 0;i<v.size();i++)
			{
				if(v[i].getMobileNum() == c[j].mobile)
				{
					c[j].vehList.push_back(v[i]);
				}
			}
		}
	}
	void Customer::acceptRecord()
		{
			string m;
			cout<<"Enter Name of Customer :		";
			getline(cin,m);
			while(m.length()==0)
				getline(cin,m);
			this->setName(m);
			cout<<"Enter Address of Customer :		";
			getline(cin,m);
			this->setAddress(m);
			cout<<"Enter Mobile of Customer :		";using namespace kd4;
			getline(cin,m);
			this->setMobile(m);
			newVehicle();
			writeInFile( *this);
		}
	ostream& operator<<(ostream &cout,Customer &other)
		{
			cout<<"Name :			"<<other.name<<endl;
			cout<<"Address :		"<<other.address<<endl;
			cout<<"Mobile :		"<<other.mobile<<endl;
			other.printVehicle();
			return cout;
		}
	istream& operator>>(istream &cin,Customer &other)
	{
		string m;
		cout<<"Enter Name of Customer :		";
		getline(cin,m);
		other.setName(m);
		cout<<"Enter Address of Customer :		";
		getline(cin,m);
		other.setAddress(m);
		cout<<"Enter Mobile of Customer :		";
		getline(cin,m);
		other.setMobile(m);
		other.newVehicle();
		other.writeInFile(other);
		return cin;
	}

}


/*
 * Customer.cpp
 *
 *  Created on: 22-Aug-2020
 *      Author: admin
 */




