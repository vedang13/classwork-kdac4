/*
 * Matrix.cpp
 *
 *  Created on: 11-Aug-2020
 *      Author: admin
 */


#include <iostream>
#include<string>
using namespace std;


namespace matrix
{

	class IllegelArgumentException
	{
	private:
		string message;
	public:
		IllegelArgumentException(string message="IllegelArgumnet"):message(message)
		{ }
		string getMessage()
		{
			return this->message;
		}
	};


	class Matrix
	{

	private:
		int row;
		int col;
		int**arr;
	public:
		Matrix():row(1),col(1)
		{
			this->arr=new int*[this->row];
			for(int i=0;i<this->row;i++)
			{
				this->arr[i]=new int[this->col];
			}

			for(int i=0;i<this->row;i++)
			{
				for(int j=0;j<this->col;j++)
				{
					this->arr[i][j]=1;
				}
			}
		}

		Matrix(int row,int col)throw(IllegelArgumentException)
		{
			if(row<0 && col<0)
				throw IllegelArgumentException("Invalid row and col size");
			else if(row<0)
				throw IllegelArgumentException("Invalid row size");
			else if(col<0)
				throw IllegelArgumentException("Invalid column size");
			else
			{
				this->row=row;
				this->col=col;

				this->arr=new int*[this->row];
				for(int i=0;i<this->row;i++)
				{
					this->arr[i]=new int[this->col];
				}

				for(int i=0;i<this->row;i++)
				{
					for(int j=0;j<this->col;j++)
					{
						this->arr[i][j]=0;
					}
				}
			}

		}

		void setRow(int row)throw(IllegelArgumentException)
		{

			if(row<0)
				throw IllegelArgumentException("Invalid row size");
			this->row=row;
		}

		void setCol(int col)throw(IllegelArgumentException)
		{
			if(col<0)
				throw IllegelArgumentException("Invalid column size");
			this->col=col;
		}

		int getElement(int row,int col)const throw(IllegelArgumentException)
		{
			if((row<0 && col<0) || (row>=this->row && col>=this->col))
				throw IllegelArgumentException("Invalid row and col size");
			else if(row<0)
				throw IllegelArgumentException("Invalid row size");
			else if(col<0)
				throw IllegelArgumentException("Invalid column size");
			else{
				return this->arr[row][col];
			}
		}

		int getCol()const throw( )
		{
			return this->col;
		}

		int getRow()const throw( )
		{
			return this->row;
		}


		void setElement(int row,int col,int element)throw(IllegelArgumentException)
		{
			if((row<0 && col<0) || (row>=this->row && col>=this->col))
				throw IllegelArgumentException("Invalid row and col size");
			else if(row<0)
				throw IllegelArgumentException("Invalid row size");
			else if(col<0)
				throw IllegelArgumentException("Invalid column size");
			else{
				 this->arr[row][col]=element;
			}
		}

		void acceptRecord()
		{
			int r,c;
			cout<<"Row:  ";
			cin>>r;
			this->setRow(r);
			cout<<"Col: ";
			cin>>c;
			this->setCol(c);

			this->arr=new int*[this->row];
			for(int i=0;i<this->row;i++)
			{
				this->arr[i]=new int[this->col];
			}

			for(int i=0;i<this->row;i++)
			{
				for(int j=0;j<this->col;j++)
				{
					cout<<"Element at["<<i<<"]["<<j<<"]: "<<endl;
					cin>>this->arr[i][j];
				}
			}

		}

		void printRecord()
		{
			for(int i=0;i<this->row;i++)
			{
				for(int j=0;j<this->col;j++)
				{
					cout<<this->arr[i][j]<<" ";
				}
				cout<<endl;
			}
		}



		Matrix addition(Matrix &other)
		{
		Matrix temp;
			if(this->row!=other.row && this->col!=other.col)
				throw IllegelArgumentException("Matrix row and column size are diffenet");
			else if(this->row!=other.row)
				throw IllegelArgumentException("Matrices has different row sizes");
			else if(this->col!=other.col)
				throw IllegelArgumentException("Matrices has different column sizes");
			else
			{
//				cout<<"Addition"<<endl;
				temp.setRow(this->row);
				temp.setCol(this->col);
				temp.arr=new int*[this->row];
				for(int i=0;i<this->row;i++)
				{

					temp.arr[i]=new int[this->col];
				}

				for(int i=0;i<this->row;i++)
				{
					//cout<<"Addition"<<i<<endl;
					for(int j=0;j<this->col;j++)
					{

						//cout<<"Addition"<<j<<endl;
						temp.arr[i][j]=this->arr[i][j] + other.arr[i][j];

					}
				}
			}
			//temp.printRecord();
			return temp;

		}

		Matrix subtraction(Matrix &other)
		{
			Matrix temp;
			if(this->row!=other.row && this->col!=other.col)
				throw IllegelArgumentException("Matrix row and column size are diffenet");
			else if(this->row!=other.row)
				throw IllegelArgumentException("Matrices has different row sizes");
			else if(this->col!=other.col)
				throw IllegelArgumentException("Matrices has different column sizes");
			else
			{
				temp.setRow(this->row);
				temp.setCol(this->col);
				temp.arr=new int*[this->row];
				for(int i=0;i<this->row;i++)
				{
					this->arr[i]=new int[this->col];
				}

				for(int i=0;i<this->row;i++)
				{
					for(int j=0;j<this->col;j++)
					{
						temp.arr[i][j]=this->arr[i][j] - other.arr[i][j];
					}
				}
			}

			return temp;
		}

		Matrix Multiplication(Matrix &other)
		{
			Matrix temp;

			if(this->col!=other.row)
				throw IllegelArgumentException("Matrix Multiplication not possible");
			temp.setRow(this->row);
			temp.setCol(other.col);

			for(int i=0;i<this->row;i++)
			{
				for(int j=0;j<this->col;j++)
				{
					int sum=0;
					sum+=this->arr[i][j] * other.arr[j][i];
					temp.arr[i][j]=sum;
				}
			}
			return temp;
		}


		Matrix ( const Matrix &other)
		{
			cout<<"		Matrix ( const Matrix &other)"<<endl;
			this->row=other.row;
			this->col=other.col;

			this->arr=new int*[this->row];

			for(int i=0;i<this->row;i++)
				this->arr[i]=new int[this->col];
			for(int i=0;i<this->row;i++)
			{
				for(int j=0;j<this->col;j++)
				{
					this->arr[i][j]=other.arr[i][j];
				}
			}
		}

		~Matrix()
		{

			if(this->arr!=NULL)
			{
				for(int i=0;i<this->row;i++)
				{
					delete[] this->arr[i];
				}
				delete[] this->arr;
				this->arr=NULL;

			}
		}
	};

}
using namespace matrix;
int main() {

	try{
		Matrix m1;
		m1.acceptRecord();
		m1.printRecord();

		Matrix m2;
		m2.acceptRecord();
		m2.printRecord();


		Matrix m3=m1.addition(m2);
		m3.printRecord();
	}
	catch(IllegelArgumentException &ex)
	{
		cout<<ex.getMessage()<<endl;
	}



	return 0;
}









