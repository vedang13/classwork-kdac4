/*
 * assi4.cpp
 *
 *  Created on: 04-Aug-2020
 *      Author: admin
 */

#include<iostream>
#define MAX 5
using namespace std;
class Stack{
private :
	int top;
	int stack[MAX];

public:

	Stack ()
{
		top=-1;
}

	void push(int element)
	{
		if(top>=MAX-1)
		{
			cout<<"stack overflow :"<<endl;
		}
		else
			--top;
		stack[top]=element;

	}

	int peek( )
		{
			return this->stack[ top ];
		}


	void pop()
	{
		if(top<=-1)
			cout<<"stack is empty "<<endl;
		else
			cout<<"popped element"<<stack[top]<<endl;
		top--;
	}

	void displayElement()
	{
		if( top >= 0)
				{
					cout<<"Stack Elements Are:";
					for(int i=top; i>=0; i--)
						cout<<stack[i]<<" ";
					cout<<endl;
				}
				else
					cout<<"Stack is empty";

	}


};

int menu_list( void )
{
	int choice ;
	cout << "\n0:Exit" << endl;
	cout << "1:Push Element In Stack:" << endl;
	cout << "2:Peek Element From Stack:" << endl;
	cout << "3:Pop Element From Stack:" << endl;
	cout << "4:Display Elements Of Stack:" << endl;
	cout << "Enter the choice:	" << endl;
	cin  >> choice;
	return choice;
}

int main( void )
{
	Stack s;
	int element , choice ;
	while( ( choice = ::menu_list() ) != 0 )
	{
		switch (choice)
		{
		case 1:
			cout << "Enter Element: ";
			cin >> element ;
			s.push( element );
			break;

		case 2:
			int peek;
			peek = s.peek( );
			cout <<"Top Element : " << peek ;
			break;

		case 3:
			s.pop();
			break;

		case 4:
			s.displayElement( );
			break;
		}
	}

	return 0;
}



