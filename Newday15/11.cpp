/*
 * 5.cpp
 *
 *  Created on: 07-Aug-2020
 *      Author: admin
 */

#include <iostream>
using namespace std;

// declare a class
class  Wall {
    public:
    Wall()
    {
   int r, c, a[100][100], i, j;

    cout << "Enter number of rows (between 1 and 100): ";
    cin >> r;

    cout << "Enter number of columns (between 1 and 100): ";
    cin >> c;

    cout << endl << "Enter elements of 1st matrix: " << endl;

    // Storing elements of first matrix entered by user.
    for(i = 0; i < r; ++i){
       for(j = 0; j < c; ++j)
       {
           cout << "Enter element a" << i + 1 << j + 1 << " : ";
           cin >> a[i][j];
       }
    }

    //printing logic
    for(i = 0; i < r; ++i){
       for(j = 0; j < c; ++j)
       {
           cout << a[i][j];

       }
       cout<<endl;
    }
    }
};

int main() {

    // create an object
    Wall wall1;

    return 0;
}


