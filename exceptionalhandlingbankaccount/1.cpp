/*
 * 1.cpp
 *
 *  Created on: 06-Aug-2020
 *      Author: admin
 */



#include<iostream>
#include<string>
using namespace std;

class InvalidAccountNumberException
{
private:
    string message;
public:
    InvalidAccountNumberException(const string msg) throw() : message(msg)
    {}

    string getMessage() const throw()
    {
        return this->message;
    }
};

class InsufficientBalanceException
{
private:
    string message;
public:
    InsufficientBalanceException(const string msg) throw() : message(msg)
    {}

    string getMessage() const throw()
    {
        return this->message;
    }
};

class IllegalArgumentException      // bal -ve enter nhi hna h
{
private:
    string message;
public:
    IllegalArgumentException(const string msg) throw () :  message(msg)
    {}
    string getMessage() const throw()
    {
        return this->message;
    }
};

class Account
{
    string name;
    int number;
    string type;
    float balance;
public:

    void setNumber(int count) throw()
    {
        this->number= ++count;
    }

    int getNumber()const throw(){
        return this->number;
    }
    void deposit(int amount) throw(InvalidAccountNumberException)
    {
        this->balance=this->balance+amount;
    }
    void withdraw(int amount)throw(InvalidAccountNumberException, InsufficientBalanceException)
    {
        this->balance-=amount;
    }
    int getBalance()const throw()
    {
        return this->balance;
    }

    void setBalance(float balance) throw()
    {
        this->balance = balance;
    }

    string getName() const throw()
    {
        return this->name;
    }

    void setName( string name) throw()
    {
        this->name = name;
    }


    string getType() const throw() {
        return this->type;
    }

    void setType( string type)throw() {
        this->type = type;
    }
};
int count = 1000;
class Bank
{
    int index;
    Account arr[ 5 ];
public:
    Bank(int index){
        this->index=index;
    }
    void create_account(Account *account ) throw()
    {
        account->setNumber(count);
        count++;
        this->arr[ ++ this->index ] = *account;
    }
    float deposit(  int number, float amount ) throw (InvalidAccountNumberException)
    {
        for( int index = 0; index <= this->index; ++ index )
        {
            if( this->arr[ index ].getNumber() == number  )
            {
                this->arr[ index ].deposit(amount) ;

                return this->arr[ index ].getBalance();
            }
                    throw  InvalidAccountNumberException("Invaid account number");
        }

        return 0;
    }
    float withdraw(  int number, float amount ) throw (InvalidAccountNumberException, InsufficientBalanceException)

    {
        for( int index = 0; index <= this->index; ++ index )
        {
            if( this->arr[ index ].getNumber() == number  )
            {
                if((this->arr[index].getBalance() - amount) < 0)
                    throw InsufficientBalanceException("Insufficient balance.");

                this->arr[ index ].withdraw(amount);
                return this->arr[ index ].getBalance();
            }
                                throw  InvalidAccountNumberException("Invaid account number");
        }

        return 0;
    }
    Account get_account_details(  int number )throw()
    {
        Account account ;
        for( int index = 0; index <= this->index; ++ index )
        {
            if( arr[ index ].getNumber() == number  )
            {
                account = arr[ index ];
            }
        }
        return account;
    }
};

void accept_account_info( Account *ptr ) throw (IllegalArgumentException)
{
    string name;
    string type;
    float balance;
    cout<<"Name	:	";
    cin>>name ;
    ptr->setName(name);

    cout<<"Type	:	";
    cin>>type;
    ptr->setType(type);

    cout<<"Balance	:	";
    cin>>balance ;
    if(balance<0)
        throw IllegalArgumentException("Balance can't be a -ve value");
    ptr->setBalance(balance);

}
void print_account_number( Account *ptr  )throw()
{
    cout<<"Account number	:	"<< ptr->getNumber()<<endl;
}


void accept_account_number( int *number ) throw()
{
    cout<<"Account number	:	";
    cin>> *number ;
}
void accept_amount( float *amount )throw()
{
    cout<<"Enter Amount	:	";
    cin>>*amount;
}

void print_balance( float balance )throw()
{
    cout<<"Balance	:	"<< balance <<endl;
}
int menu_list( void )
{
    int choice;
    cout<<"0.Exit"<<endl;
    cout<<"1.Create New Account"<<endl;
    cout<<"2.Deposit"<<endl;
    cout<<"3.Withdraw"<<endl;
    cout<<"4.Print Account details"<<endl;
    cout<<"Enter choice	:	"<<endl;
    cin>>choice;
    return choice;
}
void print_account_info( Account *ptr )
{
    cout<<endl;
    cout<<ptr->getName()<<"	"<<ptr->getNumber()<<"	"<<ptr->getType()<<"	"<<ptr->getBalance()<<endl;

}
int main( void )
{
    int choice, accNumber;
    float balance, amount;
    Bank bank(-1);
    Account account;
    while( ( choice = menu_list( ) ) )
    {
        try
        {
            switch( choice )
            {
                case 1:
                    accept_account_info(&account);
                    bank.create_account( &account);
                    print_account_number(&account);
                    break;
                case 2:
                    accept_account_number(&accNumber);
                    accept_amount( &amount );
                    balance = bank.deposit( accNumber, amount );
                    print_balance(balance);
                    break;
                case 3:
                    accept_account_number(&accNumber);
                    accept_amount(&amount );
                    balance = bank.withdraw( accNumber, amount );
                    print_balance(balance);
                    break;
                case 4:
                    accept_account_number(&accNumber);
                    account = bank.get_account_details(accNumber);
                    print_account_info(&account);
                    break;
            }
        }
        catch(IllegalArgumentException e)
        {
            cout<<e.getMessage()<<endl;
        }
        catch(InvalidAccountNumberException e)
        {
            cout<<e.getMessage()<<endl;
        }
        catch(InsufficientBalanceException e)
        {
            cout<<e.getMessage()<<endl;
        }

    }

    return 0;
}
